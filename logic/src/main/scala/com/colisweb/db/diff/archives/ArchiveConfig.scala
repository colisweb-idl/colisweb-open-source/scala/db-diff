package com.colisweb.db.diff.archives

import com.colisweb.db.diff.db.ConnectionConfig
import pureconfig.ConfigSource
import pureconfig.generic.auto._

/**
  * Mapped to [[https://github.com/lightbend/config Hocon]] configuration in src/main/resources
  */
case class ArchiveConfig(
    exportConnection: ConnectionConfig,
    importConnection: ConnectionConfig,
    tables: List[TableConfig] = Nil
) {
  def tableNames: Seq[String] = tables.map(_.tableName.toLowerCase)

  def tableConfig(tableName: String): Option[TableConfig] =
    tables.find(_.tableName.toLowerCase == tableName.toLowerCase)

}

object ArchiveConfig {

  def apply(service: String): ArchiveConfig =
    ConfigSource.default.at(service).at("archive").loadOrThrow[ArchiveConfig]
}
