package com.colisweb.db.diff.schemas

import com.colisweb.db.diff.db.ConnectionConfig
import pureconfig._
import pureconfig.generic.auto._

/**
  * Mapped to [[https://github.com/lightbend/config Hocon]] configuration in src/main/resources
  *
  * Default behaviour is to generate the schema for every table.
  */
case class SchemaConfig(
    connection: ConnectionConfig,
    tables: Seq[String] = Nil
)

object SchemaConfig {

  def apply(service: String, environment: String): SchemaConfig =
    ConfigSource.default.at(service).at(environment).at("schema").loadOrThrow[SchemaConfig]
}
