package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.SelectQueries._
import com.colisweb.db.diff.db.metadata._
import com.colisweb.db.diff.exports.{ColumnFilter, TableConfig, TableFilter}

import java.sql.{Connection, PreparedStatement, ResultSet, ResultSetMetaData}
import scala.util.chaining.scalaUtilChainingOps

trait SelectQueries { self: RepositoryCommon =>

  def sqlSelect(query: String): RowIterator = {
    val c         = connection()
    val statement = c.prepareStatement(query)
    if (config.engine == MySql)
      //prevent MySQL from fetching all data into memory
      statement.setFetchSize(Integer.MIN_VALUE)
    //TODO : check that PostgreSQL properly lazy loads data

    val resultSet = statement.executeQuery()
    val columns   = describeColumns(resultSet.getMetaData)

    val dataIterator = iterateRows(resultSet, columns.size).pipe {
      Iterator.unfold(_) { it =>
        if (it.hasNext) Some(it.next(), it)
        else {
          // We only want to close the connection when the iterator is consumed
          // We close the connection for MySQL because of https://stackoverflow.com/questions/3550263/streaming-resultset-error
          statement.close()
          c.close()
          None
        }
      }
    }

    RowIterator(dataIterator, columns, columns.head.tableName)
  }

  def selectAll(tableName: String, condition: String): RowIterator =
    sqlSelect(s"""SELECT * FROM $tableName
                 |WHERE $condition
                 |""".stripMargin)

  def select(
      columns: Seq[String],
      tableFilter: TableFilter,
      tableConfig: Option[TableConfig],
      tableMetaData: TableMetaData
  ): DataRows =
    withConnection { implicit connection =>
      val onlyFirstRow           = tableConfig.exists(_.onlyFirstRow)
      val rowsLimit: Option[Int] = if (onlyFirstRow) Some(1) else tableConfig.flatMap(_.maxRowsCount)
      val filter                 = tableConfig.fold(tableFilter)(c => tableFilter.withId(c, tableMetaData))

      rowsLimit match {
        case Some(maxRows) =>
          val rows = tableFilter.cartesianProduct.map(selectRows(columns, _, Some(maxRows)))
          DataRows.aggregate(rows).headOption.getOrElse(DataRows(filter.table))
        case None          => selectRows(columns, filter, None)
      }
    }

  def counts(tableName: String, where: Vector[(ColumnDescription, String)])(implicit connection: Connection): Int =
    withStatement(s"SELECT COUNT(*) FROM ${escape(tableName)} WHERE ${whereStatement(where)}") { countStatement =>
      setValues(countStatement, where.map(_._1), where.map(_._2))

      val rs = countStatement.executeQuery()
      rs.next()
      rs.getInt(1)
    }

  //TODO: this is only used to guess links, it could move elsewhere (but where ?)
  def selectTargets(polymorphicColumnPair: PolymorphicColumnPair): Seq[String] =
    withConnection { implicit connection =>
      withStatement(s""" SELECT DISTINCT ${escape(polymorphicColumnPair.typeColumn.name)}
                       | FROM ${escape(polymorphicColumnPair.table)}
                       | WHERE ${escape(polymorphicColumnPair.typeColumn.name)} IS NOT NULL""".stripMargin) {
        statement =>
          val resultSet = statement.executeQuery()
          resultSetAsRows(resultSet).map(polymorphicColumnPair.typeColumn.name).flatten
      }
    }

  private def selectRows(columns: Seq[String], tableFilter: TableFilter, limit: Option[Int])(implicit
      c: Connection
  ): DataRows =
    withStatement(s""" SELECT ${columns.map(escape).mkString(", ")}
                     | FROM ${escape(tableFilter.table)}
                     | ${whereInClause(tableFilter.filters)}
                     | ${limit.fold("")(l => s"LIMIT $l")}""".stripMargin) { statement =>
      filter(statement, tableFilter.filters)
      val resultSet = statement.executeQuery()
      resultSetAsRows(resultSet)
    }

  private def filter(preparedStatement: PreparedStatement, filters: List[ColumnFilter]): Unit =
    for {
      ((column, value), index) <- filters.flatMap(f => f.values.map(v => (f.column, v))).zipWithIndex
    } preparedStatement.setObject(index + 1, value, column.jdbcType)
}

object SelectQueries {

  def resultSetAsRows(resultSet: ResultSet, limit: Option[Int] = None): DataRows = {
    val metaData = resultSet.getMetaData
    val columns  = describeColumns(metaData)
    DataRows(columns.head.tableName, columns, readRows(resultSet, limit, metaData.getColumnCount))
  }

  private def readRows(resultSet: ResultSet, limit: Option[Int], columnsCount: Int): Vector[Vector[String]] =
    iterateRows(resultSet, columnsCount)
      .pipe(it => limit.fold(it)(it.take))
      .toVector

  private def iterateRows(resultSet: ResultSet, columnsCount: Int): Iterator[Vector[String]] =
    Iterator
      .unfold(resultSet) { rs =>
        if (rs.next())
          Some(
            Vector.tabulate(columnsCount) { c =>
              Option(rs.getObject(c + 1)).fold("null")(_.toString)
            },
            rs
          )
        else {
          rs.close()
          None
        }
      }

  private def describeColumns(metaData: ResultSetMetaData): Vector[ColumnDescription] =
    Vector.tabulate(metaData.getColumnCount) { c =>
      ColumnDescription(
        metaData.getColumnLabel(c + 1),
        typeName = metaData.getColumnTypeName(c + 1),
        jdbcType = metaData.getColumnType(c + 1),
        tableName = metaData.getTableName(c + 1)
      )
    }

}
