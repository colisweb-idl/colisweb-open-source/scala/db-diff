package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.metadata.ColumnDescription
import com.colisweb.db.diff.exports.ColumnFilter

import java.sql.{Connection, PreparedStatement, Types}
import scala.util.Using

trait RepositoryCommon {

  def executeQuery(sqlQuery: String)(implicit c: Connection): Unit =
    withStatement(sqlQuery)(_.executeUpdate())

  def withConnection[E](f: Connection => E): E =
    Using.resource(connection()) {
      f
    }

  def withStatement[E](sql: String)(f: PreparedStatement => E)(implicit c: Connection): E =
    Using.resource(c.prepareStatement(sql)) { s => f(s) }

  def connection: () => Connection
  def config: ConnectionConfig

  private val DATE_FIELD_LENGTH = "2015-03-19 17:21:26".length

  def whereStatement(where: Vector[(ColumnDescription, String)]): String =
    where
      .map {
        case (description, _) => s"${escape(description.name)} = ?"
      }
      .mkString(" AND ")

  def setValues(
      preparedStatement: PreparedStatement,
      columns: Seq[ColumnDescription],
      values: Seq[String]
  ): Unit =
    for {
      i    <- columns.indices
      value = values(i)
    } if (value.toLowerCase == "null")
      preparedStatement.setNull(i + 1, Types.NULL)
    else
      setValue(value, i, columns(i), preparedStatement)

  private def setValue(
      value: String,
      i: Int,
      columnDescription: ColumnDescription,
      preparedStatement: PreparedStatement
  ): Unit = {
    val columnType = columnDescription.jdbcType
    if (columnDescription.isArray) {
      val elements = value.tail.init
      val array    = if (elements.isBlank) Array.empty[AnyRef] else elements.split(",").asInstanceOf[Array[AnyRef]]
      val sqlArray = preparedStatement.getConnection.createArrayOf(columnDescription.typeName.tail, array)
      preparedStatement.setArray(i + 1, sqlArray)
    } else if (columnDescription.isTimestamp) { // MySQL does not allow "2015-03-19T17:21:26.0" for a timestamp, it expects "2015-03-19 17:21:26"
      preparedStatement.setObject(i + 1, value.take(DATE_FIELD_LENGTH).replace("T", " "), columnType)
    } else if (columnDescription.isPgEnum) { //PostgreSQL enum types are detected by JDBC as VARCHAR but need OTHER to be inserted
      preparedStatement.setObject(i + 1, value, Types.OTHER)
    } else preparedStatement.setObject(i + 1, value, columnType)
  }

  def whereInClause(filters: List[ColumnFilter]): String =
    if (filters.isEmpty) ""
    else
      "WHERE " + filters
        .map {
          case ColumnFilter(col, values) => s"${escape(col.name)} in ( ${Seq.fill(values.size)("?").mkString(", ")} )"
        }
        .mkString(" AND ")

  def escape(name: String): String =
    s"${config.engine.escapeQuote}$name${config.engine.escapeQuote}"
}
