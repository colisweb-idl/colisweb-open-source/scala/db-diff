package com.colisweb.db.diff.db.metadata

/**
  * from and to are regular expressions used to filter links.
  */
case class Link(from: String, to: String)
