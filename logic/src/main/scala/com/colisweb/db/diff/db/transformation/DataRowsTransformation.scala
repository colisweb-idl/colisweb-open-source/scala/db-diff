package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows

import scala.util.chaining.scalaUtilChainingOps

case class DataRowsTransformation(
    transformations: Seq[MigrateColumn] = Seq.empty,
    newTableName: Option[String] = None,
    keepPreviousColumns: Boolean = false
) {

  def apply(dataRows: DataRows): DataRows = {
    val keep = if (keepPreviousColumns) dataRows.columns.collect {
      case c if !transformations.flatMap(_.oldColumnName).contains(c.name) => KeepColumn(c.name)
    }
    else Vector()
    val t    = keep ++ transformations
    dataRows
      .copy(
        columns = t.map(_.description(dataRows)),
        data = dataRows.data.map(row => t.map(_.transform(dataRows.entry(row))))
      )
      .pipe(rows => newTableName.fold(rows)(rows.rename))
  }
}
