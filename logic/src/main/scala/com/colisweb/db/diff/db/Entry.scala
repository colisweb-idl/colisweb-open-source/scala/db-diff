package com.colisweb.db.diff.db

object Entry {
  type RowEntry = Map[String, String]

  implicit class RowEntryOps(entry: RowEntry) {

    def changeField(fieldName: String, transform: String => String): Map[String, String] = {
      entry.map {
        case (`fieldName`, value) => fieldName -> transform(value)
        case (key, value)         => key       -> value
      }
    }
  }
}
