package com.colisweb.db.diff.db

import java.sql.JDBCType
import com.colisweb.db.diff.db.Entry.RowEntry
import com.colisweb.db.diff.db.metadata.ColumnDescription
import com.colisweb.db.diff.db.transformation._
import com.colisweb.db.diff.exports.TableFilter
import upickle.default.write

/**
  * Holds data AND columns information so the data can be imported.
  *
  * @param parentTables if links like foreign keys were followed to get that data (purely informative)
  */
case class DataRows(
    tableName: String,
    columns: Vector[ColumnDescription] = Vector.empty,
    data: Vector[Vector[String]] = Vector.empty,
    parentTables: Seq[String] = Nil
) {
  def size: Int = data.size

  def migrate(newTableName: String, transformations: Vector[MigrateColumn]): DataRows = {
    DataRowsTransformation(transformations, Some(newTableName))(this)
  }

  def computeColumn(name: String, jdbcType: JDBCType, transformData: RowEntry => String): DataRows =
    keepAndTransform(CreateColumn(name, jdbcType, transformData))

  def addColumn(name: String, jdbcType: JDBCType, defaultValue: => Any = ""): DataRows =
    computeColumn(name, jdbcType, _ => defaultValue.toString)

  def renameColumn(from: String, to: String): DataRows =
    keepAndTransform(RenameColumn(from, to))

  def transformColumn(name: String, f: String => String): DataRows =
    keepAndTransform(TransformColumn(name, f))

  def rename(newTableName: String): DataRows =
    copy(
      tableName = newTableName,
      columns = columns.map(_.copy(tableName = newTableName))
    )

  lazy val columnNames: Vector[String] = columns.map(_.name.toLowerCase)

  def isEmpty: Boolean = columns.isEmpty || data.isEmpty

  def replace(columnName: String, value: String): DataRows =
    copy(data = data.map(_.updated(columnIndex(columnName), value)))

  def deleteColumn(name: String): DataRows =
    columns.find(_.name == name).fold(this) { col =>
      val columnsToKeep = columnNames.filter(col.name.!=)
      filterColumns(columnsToKeep: _*)
    }

  def map(columnNames: String*): Vector[Vector[String]] = {
    val indices = columnNames.map(column => columnIndex(column)).toVector
    data.map(row => indices.map(row))
  }

  def map(f: RowEntry => RowEntry): DataRows =
    copy(data = data.map(vector => columnNames.map(f(entry(vector)))))

  def values(columnName: String): Vector[String] = {
    val index = columnIndex(columnName)
    data.map(row => row(index))
  }

  def entry(dataRow: Vector[String]): RowEntry =
    columnNames.map(columnName => columnName -> dataRow(columnIndex(columnName))).toMap

  def filter(f: RowEntry => Boolean): DataRows =
    copy(data = data.filter(vector => f(entry(vector))))

  //This method is faster than previous (entry method is costly because it builds a Map)
  def filterV(f: Vector[String] => Boolean): DataRows =
    copy(data = data.filter(f))

  private def keepAndTransform(transformations: MigrateColumn*): DataRows =
    DataRowsTransformation(transformations.toVector, keepPreviousColumns = true)(this)

  def filterColumns(columnNames: String*): DataRows = {
    val indices      = columnNames.map(columnIndex)
    val filteredData = data.map(row =>
      row.zipWithIndex.collect {
        case (cell, i) if indices.contains(i) => cell
      }
    )
    copy(columns = columns.filter(c => columnNames.contains(c.name)), data = filteredData)
  }

  def groupBy(columnName: String): Map[String, DataRows] = {
    val keys = map(columnName).flatten
    val ci   = columnIndex(columnName)
    keys.map(key => key -> filterV(_(ci) == key)).toMap
  }

  lazy val entries: Vector[Map[String, String]] =
    data.map(row => columnNames.map(columnName => columnName -> row(columnIndex(columnName))).toMap)

  private val columnIndicesCache                = collection.mutable.Map.empty[String, Int]

  def columnIndex(column: String): Int =
    columnIndicesCache.getOrElseUpdate(
      column, {
        val predicate: ColumnDescription => Boolean = cd =>
          column.split("\\.") match {
            case Array(col)        =>
              cd.name.toLowerCase == col.toLowerCase
            case Array(table, col) =>
              cd.name.toLowerCase == col.toLowerCase && cd.tableName.toLowerCase == table.toLowerCase
          }

        columns.indexWhere(predicate) match {
          case -1 => throw new RuntimeException(s"unknown $column for $tableName, known are $columns")
          case c  => c
        }
      }
    )

  def column(name: String): ColumnDescription = columns(columnIndex(name))

  def columns(names: List[String]): Vector[ColumnDescription] = names.map(column).toVector

  object relevant {

    def unapply(tableFilter: TableFilter): Option[DataRows] =
      if (tableFilter.table.toUpperCase != tableName.toUpperCase()) None
      else {
        def acceptsRow(row: Vector[String]): Boolean =
          tableFilter.filters.forall(f => f.values.contains(row(columnIndex(f.column.name))))

        val filtered = copy(data = data.filter(acceptsRow))
        if (filtered.isEmpty) None else Some(filtered)
      }
  }

  def toJson: String =
    write(this, indent = 2)
      .replaceAll("\\\\n", "\n\\\\n")
      .replaceAll("\\\\t", "\t\\\\t")

  def flatMapRows(transformData: RowEntry => Vector[RowEntry]): DataRows =
    copy(data = data.flatMap { row =>
      val newRows = transformData(entry(row))
      newRows.map(newRow => columnNames.map(newRow))
    })

  /**
    * Similar to VLOOKUP / RECHERCHEV in Excel/Gsheet.
    */
  def lookUp(keyColumn: String, value: String, resultColumn: String): Option[String] =
    data.collectFirst { case vector if entry(vector)(keyColumn) == value => entry(vector)(resultColumn) }

}

object DataRows {
  import upickle.default._

  implicit val readWriter: ReadWriter[DataRows] = macroRW[DataRows]

  def aggregate(drs: Seq[DataRows]): List[DataRows] =
    drs
      .groupBy(_.tableName)
      .toList
      .flatMap {
        case (table, rows) =>
          rows.find(!_.isEmpty) map { relevant =>
            DataRows(
              tableName = table,
              columns = relevant.columns,
              data = rows.toVector.flatMap(_.data).distinct,
              parentTables = rows.flatMap(_.parentTables).distinct
            )
          }
      }

}
