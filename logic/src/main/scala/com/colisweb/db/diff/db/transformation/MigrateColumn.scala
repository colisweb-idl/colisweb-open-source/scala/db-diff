package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.db.metadata.ColumnDescription

trait MigrateColumn {
  def description(rows: DataRows): ColumnDescription

  def transform(data: Map[String, String]): String

  def oldColumnName: Option[String] = None
}
