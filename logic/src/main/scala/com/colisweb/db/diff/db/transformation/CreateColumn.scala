package com.colisweb.db.diff.db.transformation

import com.colisweb.db.diff.db.DataRows
import com.colisweb.db.diff.db.metadata.ColumnDescription

import java.sql.JDBCType

final case class CreateColumn(
    columnName: String,
    jdbcType: JDBCType,
    typeName: String,
    transformData: Map[String, String] => String
) extends MigrateColumn {

  override def description(rows: DataRows): ColumnDescription =
    ColumnDescription(name = columnName, jdbcType = jdbcType, typeName = typeName, tableName = rows.tableName)

  override def transform(data: Map[String, String]): String = transformData(data)

}

object CreateColumn {

  def apply(
      columnName: String,
      jdbcType: JDBCType,
      transformData: Map[String, String] => String
  ): CreateColumn =
    CreateColumn(
      columnName = columnName,
      jdbcType = jdbcType,
      typeName = jdbcType.getName,
      transformData = transformData
    )
}
