package com.colisweb.db.diff.db

import java.sql.Connection

class Repository(val connection: () => Connection, val config: ConnectionConfig)
    extends InsertQueries
    with SelectQueries
    with UpdateQueries
    with RepositoryCommon
