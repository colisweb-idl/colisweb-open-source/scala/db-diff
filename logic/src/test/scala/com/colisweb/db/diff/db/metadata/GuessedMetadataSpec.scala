package com.colisweb.db.diff.db.metadata

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class GuessedMetadataSpec extends AnyFlatSpec with Matchers {

  it should "match the singular form of groceries" in {
    val re = GuessedMetadata.singular("groceries").r
    re.matches("groceries") shouldBe true
    re.matches("grocery") shouldBe true

  }

  it should "match the singular form of addresses" in {
    val re = GuessedMetadata.singular("addresses").r
    re.matches("addresses") shouldBe true
    re.matches("address") shouldBe true
  }

  it should "match the singular form of orders" in {
    val re = GuessedMetadata.singular("orders").r
    re.matches("orders") shouldBe true
    re.matches("order") shouldBe true
  }

  it should "match the singular form of schedules" in {
    val re = GuessedMetadata.singular("schedules").r
    re.matches("schedules") shouldBe true
    re.matches("schedule") shouldBe true
  }

  it should "match the singular form of working_schedules" in {
    val re = GuessedMetadata.singular("working_schedules").r
    re.matches("working_schedules") shouldBe true
    re.matches("working_schedule") shouldBe true
  }

}
