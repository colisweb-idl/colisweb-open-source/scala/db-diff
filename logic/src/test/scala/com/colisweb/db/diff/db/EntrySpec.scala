package com.colisweb.db.diff.db

import com.colisweb.db.diff.db.Entry._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class EntrySpec extends AnyFlatSpec with Matchers {

  "Entry" should "have field changed with constant value" in {
    val entry: RowEntry = Map("name" -> "Imany", "age" -> "35")
    entry.changeField("age", _ => "42") shouldBe Map("name" -> "Imany", "age" -> "42")
  }

  "Entry" should "have field changed with function" in {
    val entry: RowEntry = Map("name" -> "Janelle Monae", "age" -> "35")
    entry.changeField("age", age => (age.toInt + 10).toString) shouldBe Map("name" -> "Janelle Monae", "age" -> "45")
  }
}
