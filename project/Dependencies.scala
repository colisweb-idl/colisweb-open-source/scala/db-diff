import sbt._

object Versions {
  val approvals          = "1.3.1"
  val flyway             = "8.2.0"
  val h2                 = "1.4.200"
  val inflector          = "1.0.14"
  val logback            = "1.5.17"
  val mainargs           = "0.7.6"
  val mysql              = "9.2.0"
  val osLib              = "0.11.4"
  val p6spy              = "3.9.1"
  val postgres           = "42.7.5"
  val pprint             = "0.9.0"
  val pureconfig         = "0.17.8"
  val sqlite             = "3.49.1.0"
  val testContainer      = "1.20.5"
  val testContainerScala = "0.41.8"
  val upickle            = "4.1.0"
  val zio                = "2.1.16"
}

object Dependencies {

  final lazy val approvalsScala           = "com.colisweb"          %% "approvals-scala"            % Versions.approvals
  final lazy val flywayCore               = "org.flywaydb"           % "flyway-core"                % Versions.flyway
  final lazy val h2                       = "com.h2database"         % "h2"                         % Versions.h2
  final lazy val inflector                = "com.hypertino"         %% "inflector"                  % Versions.inflector
  final lazy val logbackClassic           = "ch.qos.logback"         % "logback-classic"            % Versions.logback
  final lazy val mainargs                 = "com.lihaoyi"           %% "mainargs"                   % Versions.mainargs
  final lazy val mysqlConnector           = "com.mysql"              % "mysql-connector-j"          % Versions.mysql
  final lazy val osLib                    = "com.lihaoyi"           %% "os-lib"                     % Versions.osLib
  final lazy val p6spy                    = "p6spy"                  % "p6spy"                      % Versions.p6spy
  final lazy val postgresql               = "org.postgresql"         % "postgresql"                 % Versions.postgres
  final lazy val pprint                   = "com.lihaoyi"           %% "pprint"                     % Versions.pprint
  final lazy val pureconfig               = "com.github.pureconfig" %% "pureconfig"                 % Versions.pureconfig
  final lazy val sqliteJdbc               = "org.xerial"             % "sqlite-jdbc"                % Versions.sqlite
  final lazy val testContainerJdbc        = "org.testcontainers"     % "jdbc"                       % Versions.testContainer
  final lazy val testContainerMysql       = "org.testcontainers"     % "mysql"                      % Versions.testContainer
  final lazy val testContainerPostgresql  = "org.testcontainers"     % "postgresql"                 % Versions.testContainer
  final lazy val testcontainers           = "org.testcontainers"     % "testcontainers"             % Versions.testContainer
  final lazy val testcontainersScalaMysql = "com.dimafeng"          %% "testcontainers-scala-mysql" % Versions.testContainerScala

  final lazy val testcontainersScalaPostgresql =
    "com.dimafeng" %% "testcontainers-scala-postgresql" % Versions.testContainerScala

  final lazy val testcontainersScalaScalatest =
    "com.dimafeng" %% "testcontainers-scala-scalatest" % Versions.testContainerScala
  final lazy val upickle   = "com.lihaoyi" %% "upickle"     % Versions.upickle
  final lazy val zioSreams = "dev.zio"     %% "zio-streams" % Versions.zio
}
